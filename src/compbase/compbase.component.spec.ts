import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompbaseComponent } from './compbase.component';

describe('CompbaseComponent', () => {
  let component: CompbaseComponent;
  let fixture: ComponentFixture<CompbaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompbaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
